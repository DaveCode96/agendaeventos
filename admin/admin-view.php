<p class="lead">
    Usted eligio la administracion de los usuarios con privilegio administrador.
</p>
<ul class="breadcrumb" style="margin-bottom: 5px;">
    <li>
        <a href="configAdmin.php?view=admin">
            <i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp; Nuevo Administrador
        </a>
    </li>
    <li>
        <a href="configAdmin.php?view=adminlist"><i class="fa fa-list-ol" aria-hidden="true"></i> &nbsp; Administradores del sistema</a>
    </li>
    <li>
        <a href="configAdmin.php?view=account"><i class="fa fa-address-card" aria-hidden="true"></i> &nbsp; Mi cuenta</a>
    </li>
</ul>
<div class="container">
	<div class="row">
        <div class="col-xs-12">
            <div class="container-form-admin">
                <h3 class="text-info text-center">Agregar un nuevo administrador</h3>
                <form action="process/regAdmin.php" method="POST" role="form" class="FormCatElec" data-form="save">

 <div class="container-fluid">
                        <div class="row">

                              <div class="col-xs-12">
                                <legend><i class="fa fa-user"></i> &nbsp; Datos personales</legend>
                              </div>
                  
                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-user"></i>&nbsp; Ingrese sus nombres</label>
                                  <input class="form-control" type="text" required name="admin-fullname" title="Ingrese sus nombres (solamente letras)" pattern="[a-zA-Z ]{1,50}" maxlength="50">
                                </div>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-user"></i>&nbsp; Ingrese sus apellidos</label>
                                  <input class="form-control" type="text" required name="admin-lastname" title="Ingrese sus apellido (solamente letras)" pattern="[a-zA-Z ]{1,50}" maxlength="50">
                                </div>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-mobile"></i>&nbsp; Ingrese número telefónico</label>
                                    <input class="form-control" type="tel" required name="admin-phone" maxlength="15" title="Ingrese número telefónico. Mínimo 8 digitos máximo 15">
                                </div>
                              </div>

                                 <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-mobile"></i>&nbsp; Ingrese Extension telefónica</label>
                                    <input class="form-control" type="tel" required name="admin-extension" maxlength="3" title="Ingrese Extension telefónica. Mínimo  digitos máximo 3">
                                </div>
                              </div>

                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp; Ingrese su Email</label>
                                    <input class="form-control" type="email" required name="admin-email" title="Ingrese la dirección de su Email" maxlength="50">
                                </div>
                              </div>

                               <div class="col-xs-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-user"></i>&nbsp; Ingrese su Cargo </label>
                                  <input class="form-control" type="text" required name="admin-cargo" title="Ingrese su cargo actual en la universidad" maxlength="50">
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-home"></i>&nbsp; Ingrese su Ubicacion</label>
                                  <input class="form-control" type="text" required name="admin-dir" title="Ingrese la direción en la reside actualmente" maxlength="100">
                                </div>
                              </div>


                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre de usuario</label>
                                    <input class="form-control" type="text" name="admin-name" maxlength="9" pattern="[a-zA-Z0-9]{4,9}" required="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Ingrese la contraseña</label>
                                    <input class="form-control" type="password" name="admin-pass1" required="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Repita la contraseña</label>
                                    <input class="form-control" type="password" name="admin-pass2" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="text-center"><button type="submit" class="btn btn-primary btn-raised">Agregar administrador</button></p>
                </form>
            </div>
        </div>
    </div>
</div>