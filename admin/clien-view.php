<p class="lead">
    Administracion de usuarios coordinadores de eventos.
</p>
<ul class="breadcrumb" style="margin-bottom: 5px;">
    <li>
        <a href="configAdmin.php?view=clien">
            <i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp; Nuevo Usuario
        </a>
    </li>
    <li>
        <a href="configAdmin.php?view=clienlist"><i class="fa fa-list-ol" aria-hidden="true"></i> &nbsp; Usuarios coordinadores de eventos</a>
    </li>

</ul>
<div class="container">
	<div class="row">
        <div class="col-xs-12">
            <div class="container-form-admin">
                <h3 class="text-info text-center">Agregar un nuevo usuario coordinador</h3>
                <form action="process/regclien.php" method="POST" role="form" class="FormCatElec" data-form="save">



                    <div class="container-fluid">
                        <div class="row">

                              <div class="col-xs-12">
                                <legend><i class="fa fa-user"></i> &nbsp; Datos personales</legend>
                              </div>
                              <div class="col-xs-12">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp; Ingrese su número de Identificacion(unico)</label>
                                  <input class="form-control" type="text" required name="clien-nit" pattern="[0-9]{1,15}" title="Ingrese su número de identificacion. Solamente números" maxlength="15" >
                                </div>   

   </div>
                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-user"></i>&nbsp; Ingrese sus nombres</label>
                                  <input class="form-control" type="text" required name="clien-fullname" title="Ingrese sus nombres (solamente letras)" pattern="[a-zA-Z ]{1,50}" maxlength="50">
                                </div>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-user"></i>&nbsp; Ingrese sus apellidos</label>
                                  <input class="form-control" type="text" required name="clien-lastname" title="Ingrese sus apellido (solamente letras)" pattern="[a-zA-Z ]{1,50}" maxlength="50">
                                </div>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-mobile"></i>&nbsp; Ingrese número telefónico</label>
                                    <input class="form-control" type="tel" required name="clien-phone" maxlength="15" title="Ingrese número telefónico. Mínimo 8 digitos máximo 15">
                                </div>
                              </div>

                                 <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-mobile"></i>&nbsp; Ingrese Extension telefónica</label>
                                    <input class="form-control" type="tel" required name="clien-extension" maxlength="3" title="Ingrese Extension telefónica. Mínimo  digitos máximo 3">
                                </div>
                              </div>

                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp; Ingrese su Email</label>
                                    <input class="form-control" type="email" required name="clien-email" title="Ingrese la dirección de su Email" maxlength="50">
                                </div>
                              </div>

                               <div class="col-xs-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-user"></i>&nbsp; Ingrese su Cargo </label>
                                  <input class="form-control" type="text" required name="clien-cargo" title="Ingrese su cargo actual en la universidad" maxlength="50">
                                </div>
                              </div>

                              <div class="col-xs-12">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-home"></i>&nbsp; Ingrese su Ubicacion</label>
                                  <input class="form-control" type="text" required name="clien-dir" title="Ingrese la direción en la reside actualmente" maxlength="100">
                                </div>
                              </div>


                                <div class="col-xs-12">
                                <legend><i class="fa fa-lock"></i> &nbsp; Datos de la cuenta</legend>
                              </div>
                              <div class="col-xs-12">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp; Ingrese su nombre de usuario</label>
                                    <input class="form-control" type="text" required name="clien-name" title="Ingrese su nombre. Máximo 9 caracteres (solamente letras y numeros sin espacios)" pattern="[a-zA-Z0-9]{1,9}" maxlength="9">
                                </div>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-lock"></i>&nbsp; Introduzca una contraseña</label>
                                  <input class="form-control" type="password" required name="clien-pass" title="Defina una contraseña para iniciar sesión">
                                </div>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label"><i class="fa fa-lock"></i>&nbsp; Repita la contraseña</label>
                                    <input class="form-control" type="password" required name="clien-pass2" title="Repita la contraseña">
                                </div>
                              </div>
                            </div>


                    </div>
                    <p class="text-center"><button type="submit" class="btn btn-primary btn-raised">Agregar Usuario</button></p>
                </form>
            </div>
        </div>
    </div>
</div>