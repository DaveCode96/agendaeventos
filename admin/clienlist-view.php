<p class="lead">
    Lista de usuarios coordinadores de eventos en el sistema actualmente.
</p>
<ul class="breadcrumb" style="margin-bottom: 5px;">
    <li>
        <a href="configAdmin.php?view=clien">
            <i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp; Nuevo Usuario
        </a>
    </li>
    <li>
        <a href="configAdmin.php?view=clienlist"><i class="fa fa-list-ol" aria-hidden="true"></i> &nbsp; Usuarios coordinadores de eventos</a>
    </li>

</ul>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <br><br>
            <div class="panel panel-info">
                <div class="panel-heading text-center"><h4>Usuarios Coordinadores</h4></div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="">
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Nombre(s)</th>
                                <th class="text-center">Apellido(s)</th>
                                <th class="text-center">Direccion</th>
                                <th class="text-center">Telefono</th>
                                <th class="text-center">Extension</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Cargo</th>
                                <th class="text-center">Usuario</th>
                                <th class="text-center">Identificacion(#)</th>
                                <th class="text-center">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $mysqli = mysqli_connect(SERVER, USER, PASS, BD);
                                mysqli_set_charset($mysqli, "utf8");

                                $pagina = isset($_GET['pag']) ? (int)$_GET['pag'] : 1;
                                $regpagina = 30;
                                $inicio = ($pagina > 1) ? (($pagina * $regpagina) - $regpagina) : 0;

                                $usuarios=mysqli_query($mysqli,"SELECT SQL_CALC_FOUND_ROWS * FROM cliente WHERE NIT!='1' LIMIT $inicio, $regpagina");

                                $totalregistros = mysqli_query($mysqli,"SELECT FOUND_ROWS()");
                                $totalregistros = mysqli_fetch_array($totalregistros, MYSQLI_ASSOC);

                                $numeropaginas = ceil($totalregistros["FOUND_ROWS()"]/$regpagina);

                                $cr=$inicio+1;
                              while($cli=mysqli_fetch_array($usuarios, MYSQLI_ASSOC)){
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $cr; ?></td>
                                <td class="text-center"><?php echo $cli['NombreCompleto']; ?></td>
                                <td class="text-center"><?php echo $cli['Apellido']; ?></td>
                                <td class="text-center"><?php echo $cli['Direccion']; ?></td>
                                <td class="text-center"><?php echo $cli['Telefono']; ?></td>
                                <td class="text-center"><?php echo $cli['Extension']; ?></td>
                                <td class="text-center"><?php echo $cli['Email']; ?></td>
                                <td class="text-center"><?php echo $cli['Cargo']; ?></td>
                                <td class="text-center"><?php echo $cli['Nombre']; ?></td>
                                <td class="text-center"><?php echo $cli['NIT']; ?></td>
                                <td class="text-center">
                                    <form action="process/delclien.php" method="POST" class="FormCatElec" data-form="delete">
                                        <input type="hidden" name="clien-code" value="<?php echo $cli['NIT']; ?>">
                                        <button type="submit" class="btn btn-raised btn-xs btn-danger">Eliminar</button>    
                                    </form>
                                </td>
                            </tr>
                            <?php
                                $cr++;
                              }
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php if($numeropaginas>=1): ?>
                <div class="text-center">
                  <ul class="pagination">
                    <?php if($pagina == 1): ?>
                        <li class="disabled">
                            <a>
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="configAdmin.php?view=clienlist&pag=<?php echo $pagina-1; ?>">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                    <?php endif; ?>


                    <?php
                        for($i=1; $i <= $numeropaginas; $i++ ){
                            if($pagina == $i){
                                echo '<li class="active"><a href="configAdmin.php?view=clienlist&pag='.$i.'">'.$i.'</a></li>';
                            }else{
                                echo '<li><a href="configAdmin.php?view=clienlist&pag='.$i.'">'.$i.'</a></li>';
                            }
                        }
                    ?>
                    

                    <?php if($pagina == $numeropaginas): ?>
                        <li class="disabled">
                            <a>
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="configAdmin.php?view=clienlist&pag=<?php echo $pagina+1; ?>">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    <?php endif; ?>
                  </ul>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>