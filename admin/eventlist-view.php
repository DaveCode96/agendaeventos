<p class="lead">
	Lista y administracion de los eventos creados por los usuarios.
</p>
<ul class="breadcrumb" style="margin-bottom: 5px;">
    <li>
        <a href="configAdmin.php?view=event">
            <i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp; Nuevo evento
        </a>
    </li>
    <li>
        <a href="configAdmin.php?view=eventlist"><i class="fa fa-list-ol" aria-hidden="true"></i> &nbsp; Eventos del calendario</a>
    </li>
</ul>
<div class="container">
	<div class="row">
		
    <div class="col-xs-12">
            <br><br>
            <div class="panel panel-info">
              <div class="panel-heading text-center"><h4>Eventos del calendario</h4></div>
                <div class="table-responsive">
                  <table class="table table-striped table-hover">
                      <thead class="">
                          <tr>
                          	  <th class="text-center">#</th>
                              <th class="text-center">#Evento</th>
                              <th class="text-center">Nombre(s) Ponente</th>
                              <th class="text-center">Apellidos</th>
                              <th class="text-center">Procedencia</th>
                              <th class="text-center">Telefono</th>
                              <th class="text-center">Email</th>
                              <th class="text-center">Coordinador</th>
                               <th class="text-center">#Coordinador</th>
                              <th class="text-center">Actualizar</th>
                              <th class="text-center">Eliminar</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                        	$mysqli = mysqli_connect(SERVER, USER, PASS, BD);
							mysqli_set_charset($mysqli, "utf8");

							$pagina = isset($_GET['pag']) ? (int)$_GET['pag'] : 1;
							$regpagina = 30;
							$inicio = ($pagina > 1) ? (($pagina * $regpagina) - $regpagina) : 0;

							$eventos=mysqli_query($mysqli,"SELECT SQL_CALC_FOUND_ROWS * FROM eventos LIMIT $inicio, $regpagina");

							$totalregistros = mysqli_query($mysqli,"SELECT FOUND_ROWS()");
							$totalregistros = mysqli_fetch_array($totalregistros, MYSQLI_ASSOC);

							$numeropaginas = ceil($totalregistros["FOUND_ROWS()"]/$regpagina);

							$cr=$inicio+1;
                            while($prod=mysqli_fetch_array($eventos, MYSQLI_ASSOC)){
                        ?>
                        <tr>
                        	<td class="text-center"><?php echo $cr; ?></td>
                          <td class="text-center"><?php echo $prod['CodigoEvento']; ?></td>
                        	<td class="text-center"><?php echo $prod['NombrePonente']; ?></td>
                        	<td class="text-center"><?php echo $prod['ApellidoPonente']; ?></td>
                          <td class="text-center"><?php echo $prod['Procedencia']; ?></td>
                          <td class="text-center"><?php echo $prod['Telefono']; ?></td>
                          <td class="text-center"><?php echo $prod['Email']; ?></td>
                        	<td class="text-center">
                        		<?php 
                        			$categ=ejecutarSQL::consultar("SELECT NombreCompleto,Apellido FROM cliente WHERE NIT='".$prod['NIT']."'");
                        			$datc=mysqli_fetch_array($categ, MYSQLI_ASSOC);
                        			echo $datc['NombreCompleto']." ".$datc['Apellido'];
                        		?>
                        	</td>
                          <td class="text-center"><?php echo $prod['NIT']; ?></td>
                          
                     
                        
                        	<td class="text-center">
                        		<a href="configAdmin.php?view=eventinfo&code=<?php echo $prod['CodigoEvento']; ?>" class="btn btn-raised btn-xs btn-success">Actualizar</a>
                        	</td>
                        	<td class="text-center">
                        		<form action="process/delevent.php" method="POST" class="FormCatElec" data-form="delete">
                        			<input type="hidden" name="prod-codigo" value="<?php echo $prod['CodigoEvento']; ?>">
                        			<button type="submit" class="btn btn-raised btn-xs btn-danger">Eliminar</button>	
                        		</form>
                        	</td>
                        </tr>
                        <?php 
                        	$cr++;
                        	}
                        ?>
                      </tbody>
                  </table>
                </div>
                <?php if($numeropaginas>=1): ?>
              	<div class="text-center">
                  <ul class="pagination">
                    <?php if($pagina == 1): ?>
                        <li class="disabled">
                            <a>
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="configAdmin.php?view=eventlist&pag=<?php echo $pagina-1; ?>">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                    <?php endif; ?>


                    <?php
                        for($i=1; $i <= $numeropaginas; $i++ ){
                            if($pagina == $i){
                                echo '<li class="active"><a href="configAdmin.php?view=eventlist&pag='.$i.'">'.$i.'</a></li>';
                            }else{
                                echo '<li><a href="configAdmin.php?view=eventlist&pag='.$i.'">'.$i.'</a></li>';
                            }
                        }
                    ?>
                    

                    <?php if($pagina == $numeropaginas): ?>
                        <li class="disabled">
                            <a>
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="configAdmin.php?view=eventlist&pag=<?php echo $pagina+1; ?>">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    <?php endif; ?>
                  </ul>
                </div>
                <?php endif; ?>
            </div>
        </div>
	</div>
</div>