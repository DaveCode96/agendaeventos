<!DOCTYPE html>
<html lang="es">
<head>
    <title>Administrar Eventos</title>
    <?php include './inc/link.php'; ?>
</head>
<body id="container-page-index">
    <?php include './inc/navbar.php'; ?>
   


    <section id="container-pedido">
        <div class="container">
            <div class="page-header">
              <h1>Eventos <small class="tittles-pages-logo">UPT</small></h1>
            </div>
            
            
                <?php
                  require_once "library/configServer.php";
                  require_once "library/consulSQL.php";
    
                ?>
       
        </div>
        



<div class="container">
  <div class="row">
    <p class="lead">
    Bienvenido a la seccion de administracion de eventos, en esta seccion usted podra gestionar sus eventos para crearlos, eliminarlos y modificarlos.
</p>
<ul class="breadcrumb" style="margin-bottom: 5px;">
    <li>
        <a href="evento.php">
            <i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp; Nuevo Evento y lista de eventos
        </a>
    </li>

</ul>
        <div class="col-xs-12">
            <div class="container-form-admin">
                <h3 class="text-primary text-center">Agregar un evento al calendario</h3>
                <form action="./process/regevent.php" method="POST" enctype="multipart/form-data" class="FormCatElec" data-form="save">
                    <div class="container-fluid">
                        <div class="row">

  
                            <div class="col-xs-12">
                                <legend>Codigo Evento</legend>
                            </div>                           
                            <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                <label class="control-label">Código de evento(ejemplo (#evento-titulo-#identificadorusuario))</label>
                                <input type="text" class="form-control" required maxlength="30" name="prod-codigo">
                              </div>
                            </div>

                            <div class="col-xs-12">
                                <legend>Datos del ponente</legend>
                            </div>
                          



                            <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                <label class="control-label">Nombre(s) del ponente</label>
                                <input type="text" class="form-control" required maxlength="30" name="prod-name">
                              </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                <label class="control-label">Apellido(s) del ponente</label>
                                <input type="text" class="form-control" required maxlength="30" name="prod-apellido">
                              </div>
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                <label class="control-label">Procedencia(empresa)</label>
                                <input type="text" class="form-control" required name="prod-procedencia">
                              </div>
                            </div>


                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-mobile"></i>&nbsp; Ingrese número telefónico</label>
                                    <input class="form-control" type="tel" required name="prod-phone" maxlength="15" title="Ingrese número telefónico. Mínimo 8 digitos máximo 15">
                                </div>
                              </div>

                              <div class="col-xs-12 col-sm-6">
                                <div class="form-group label-floating">
                                  <label class="control-label"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp; Ingrese su Email</label>
                                    <input class="form-control" type="email" required name="prod-email" title="Ingrese la dirección de su Email" maxlength="50">
                                </div>
                              </div>



                       <!--NIP DEL USUARIO ACTUAL OCULTO y declarado como solo lectura -->
                                <input type="hidden" readonly name="prod-coordinador" value="<?php echo $_SESSION['UserNIT']; ?>">
                                
                     

                                   <div class="col-xs-12">
                                <legend>Ubicacion del evento</legend>
                              </div>


                              <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                <label class="control-label">Descripcion de ubicacion del evento</label>
                                <input type="text" class="form-control" required name="prod-ubicacion">
                              </div>
                            </div>


                           <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                <label class="control-label">Edificio ejemplo...(UD1)</label>
                                <input type="text" class="form-control" required name="prod-edificio">
                              </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                <label class="control-label">Planta ejemplo...(planta baja, aire libre, etc)</label>
                                <input type="text" class="form-control" required name="prod-planta">
                              </div>
                            </div>













                             <div class="col-xs-12">
                                <legend>Datos del evento</legend>
                              </div>


                              <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                <label class="control-label">Nombre del evento</label>
                                <input type="text" class="form-control" required name="title" id="title">
                              </div>
                            </div>


                        

                              <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                 <label class="control-label">Categoria</label>
                    <select class="form-control"   name="class" id="tipo">
                        <option value="event-info">Academico / Curso</option>
                        <option value="event-success">Cultural / Deportivo</option>
                        <option value="event-important">Conferencia / Consurso</option>
                        <option value="event-warning">Convenio / Graduacion</option>
                        <option value="event-special">Otro</option>
                    </select>
                            </div>
                            </div>


    <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                                 <label class="control-label">Descripcion del evento</label>
  <textarea id="body"  required name="event"  class="form-control" rows="3"></textarea>

 </div>
                            </div>
 <div class="col-xs-12">
                                <legend>Fecha del evento (verifique correctamente los datos)</legend>
                              </div>



<head>
     
      
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <script src="admin/moment.js"></script>
        <script src="admin/bootstrap.min.js"></script>
        <script src="admin/bootstrap-datetimepicker.js"></script>
        <link rel="stylesheet" href="admin/bootstrap-datetimepicker.min.css" />
       <script src="admin/bootstrap-datetimepicker.es.js"></script>
    

</head>

        <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                    <label for="from">Inicio</label>
                    
                        <input type='text' id="from" name="from" class="form-control" readonly />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    
  </div>
                            </div>

                    <br>
  <div class="col-xs-12 col-sm-6 col-md-4">
                              <div class="form-group label-floating">
                    <label for="to">Fin</label>
                  
                        <input type='text' name="to" id="to" class="form-control" readonly />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                   
</div>
                            </div>
                 
    <script type="text/javascript">
        $(function () {
            $('#from').datetimepicker({
                language: 'es',
                minDate: new Date()
            });
            $('#to').datetimepicker({
                language: 'es',
                minDate: new Date()
            });

        });
    </script>
  



























                            <div class="col-xs-12">
                                <legend>Imagen/Foto del Evento</legend>
                                <p class="text-center text-primary">
                                    Seleccione una imagen/foto en el siguiente campo. Formato de imágenes admitido png y jpg. Tamaño máximo 5MB
                                </p>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                  <input type="file" name="img">
                                  <div class="input-group">
                                    <input type="text" readonly="" class="form-control" placeholder="Seleccione la imagen del evento...">
                                      <span class="input-group-btn input-group-sm">
                                        <button type="button" class="btn btn-fab btn-fab-mini">
                                          <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                        </button>
                                      </span>
                                  </div>
                                    <p class="help-block">Formato de imágenes admitido png y jpg. Tamaño máximo 5MB</p>
                                </div>
                            </div>
                        </div>
                    </div>
                <input type="hidden"  name="event-name" value="<?php echo $_SESSION['nombreAdmin'] ?>">
                <p class="text-center"><button type="submit" class="btn btn-primary btn-raised">Agregar al calendario</button></p>
                </form>
            </div>
        </div>     
    </div>
</div>























        <?php
            if($_SESSION['UserType']=="User"){
                $consultaC=ejecutarSQL::consultar("SELECT * FROM eventos WHERE NIT='".$_SESSION['UserNIT']."'");
        ?>
         
            <div class="container" style="margin-top: 70px;">
              <div class="page-header">
                <h1>Mis eventos</h1>
              </div>
            </div>
        <?php
            if(mysqli_num_rows($consultaC)>=1){
        ?> 
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Codigo evento</th>
                                        <th>Nombre Completo</th>
                                        
                                        <th>Telefono</th>
                                        <th>Evento</th>
                                        <th>Ubicacion</th>
                                        <th>Inicio</th>
                                        <th>Fin</th>
                                        <th>Actualizar</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while($rw=mysqli_fetch_array($consultaC, MYSQLI_ASSOC)){
                                    ?> 
                                        <tr>
                                            <td><?php echo $rw['CodigoEvento']; ?></td>
                                            <td><?php echo $rw['NombrePonente']." ".$rw['ApellidoPonente']; ?></td> 
                                            <td><?php echo $rw['Telefono']; ?></td>
                                            <td><?php echo $rw['title']; ?></td>
                                            <td><?php echo $rw['Ubicacion']." ".$rw['Edificio'] ." ".$rw['Planta']; ?></td>
                                            <td><?php echo $rw['inicio_normal']; ?></td>
                                            <td><?php echo $rw['final_normal']; ?></td>
                                           
                                              <td class="text-center">
                            <a href="eventinfo.php?view=eventinfo&code=<?php echo $rw['CodigoEvento']; ?>" class="btn btn-raised btn-xs btn-success">Actualizar</a>
                          </td>



                          <td class="text-center">
                            <form action="process/delevent.php" method="POST" class="FormCatElec" data-form="delete">
                              <input type="hidden" name="prod-codigo" value="<?php echo $rw['CodigoEvento']; ?>">
                              <button type="submit" class="btn btn-raised btn-xs btn-danger">Eliminar</button>  
                            </form>
                              </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        <?php
            }
            mysqli_free_result($consultaC);
        }
        ?>







    </section>
    
    <div class="ResForm"></div>
    <?php include './inc/footer.php'; ?>
</body>
</html>
