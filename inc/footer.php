<div class="ResForm"></div>
<div class="ResbeforeSend"></div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h4 class="text-footer">Contactanos</h4>
                <a href="https://www.facebook.com/UPTxOficial/" target="_blank">
                    <i class="fa fa-facebook" aria-hidden="true">&nbsp; Facebook </i> 
                </a><br>
                <a href="https://twitter.com/UPTxOficial" target="_blank">
                    <i class="fa fa-twitter" aria-hidden="true">&nbsp; Twitter </i>
                </a><br>
                <a href="https://www.youtube.com/channel/UCY_jx-W3WBuGkYY1GRgBsLg" target="_blank">
                    <i class="fa fa-youtube-play" aria-hidden="true">&nbsp; YouTube </i>
                </a><br>
                <a href="https://www.instagram.com/uptxoficial/" target="_blank">
                    <i class="fa fa-instagram" aria-hidden="true">&nbsp; Instagram </i>
                </a><br>
                <a href="https://www.google.com/maps/place/Universidad+Polit%C3%A9cnica+de+Tlaxcala/@19.2328341,-98.2389092,15z/data=!4m5!3m4!1s0x0:0x1acfb151638ce925!8m2!3d19.2328341!4d-98.2389092" target="_blank">
                    <i class="fa fa-map-marker" aria-hidden="true">&nbsp; Encuentranos </i>
                </a>
            </div>
            <div class="col-sm-4">
                <h4 class="text-footer">Porque elegirnos</h4>
                <p> 
                    Tenemos la habilidad para desarrollar.</br>
                    Contamos con la inovacion.</br>
                    Contamos con la calidad.</br>
                    Cumplimos lo que prometemos.</br>
                </p>
            </div>
            <div class="col-sm-4">
                <h4 class="text-footer" >Direccion</h4>
                <p style="color: #FFF">A. Universidad Politecnica, San Pedro Xalcaltzinco, 90180 Tlax.</p>
                <p style="color: #FFF">Enrique Padilla Sánchez</p>
                <p class="fa fa-mobile" style="font-size:20px" aria-hidden="true">  246 465 1300</p>
                <p style="color: #FFF">Fundación: 10 de septiembre de 2004</p>
                
            </div>
        </div>
    </div>
    <h5 class="text-center tittles-pages-logo text-footer">UPT &copy; 2019</h5>
</footer>
