<?php
include './library/configServer.php';
include './library/consulSQL.php';
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>Eventos</title>
    <?php include './inc/link.php'; ?>
</head>

<body id="container-page-product">
    <?php include './inc/navbar.php'; ?>
    <section id="infoproduct">
        <div class="container">
            <div class="row">
                <div class="page-header">
                    <h1>DETALLE DEl EVENTO <small class="tittles-pages-logo">UPT</small></h1>
                </div>


                <?php 
                    $CodigoProducto=consultasSQL::clean_string($_GET['CodigoEvento']);
                    $productoinfo=  ejecutarSQL::consultar("SELECT eventos.CodigoEvento,eventos.title,eventos.NombrePonente,eventos.ApellidoPonente,eventos.Email,eventos.Procedencia,cliente.NombreCompleto,eventos.Ubicacion,eventos.Edificio,eventos.Planta,eventos.body,eventos.inicio_normal,eventos.final_normal,eventos.Imagen FROM cliente INNER JOIN eventos ON eventos.NIT=cliente.NIT  WHERE CodigoEvento='".$CodigoProducto."'");
                    while($fila=mysqli_fetch_array($productoinfo, MYSQLI_ASSOC)){
                        echo '
                            <div class="col-xs-12 col-sm-6">
                                <h3 class="text-center">Información del evento</h3>
                                <br><br>

                                <h4><strong>Evento: </strong>'.$fila['title'].'</h4>
                                <h4><strong>Nombre Ponente: </strong>'.$fila['NombrePonente']." ".$fila['ApellidoPonente'].'</h4><br>
                                <h4><strong>Email: </strong>'.$fila['Email'].'</h4>
                                <h4><strong>Ubicacion: </strong>'.$fila['Ubicacion']." ".$fila['Edificio']." ".$fila['Planta'].'</h4><br>
                                 <h4><strong>Fecha de inicio: </strong>'.$fila['inicio_normal'].'</h4><br>
                                  <h4><strong>Fecha de finalizacion: </strong>'.$fila['final_normal'].'</h4><br>

                                <h4><strong>Procedencia: </strong>'.$fila['Procedencia'].'</h4><br>

                                <h4><strong>Codigo del evento: </strong>'.$fila['CodigoEvento'].'</h4>
                                <h4><strong>Descripcion del evento: </strong>'.$fila['body'].'</h4><br>';
                                
                                if($fila['Imagen']!="" && is_file("./assets/img-products/".$fila['Imagen'])){ 
                                    $imagenFile="./assets/img-products/".$fila['Imagen']; 
                                }else{ 
                                    $imagenFile="./assets/img-products/default.png"; 
                                }
                                echo '<br>
                                <a href="index.php" class="btn btn-lg btn-primary btn-raised btn-block"><i class="fa fa-mail-reply"></i>&nbsp;&nbsp;Regresar al Inicio</a>
                            </div>


                            <div class="col-xs-12 col-sm-6">
                                <br><br><br>
                                <img class="img-responsive" src="'.$imagenFile.'">
                            </div>';
                    }
                ?>
            </div>
        </div>
    </section>

    <?php include './inc/footer.php'; ?>

</body>

</html>