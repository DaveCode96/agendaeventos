<?php
session_start();
include '../library/configServer.php';
include '../library/consulSQL.php';

$codeclien=consultasSQL::clean_string($_POST['clien-code']);
if(consultasSQL::DeleteSQL('cliente', "NIT='".$codeclien."'")){
    echo '<script>
	    swal({
	      title: "Usuario eliminado",
	      text: "El Usuario estandar se eliminó con éxito",
	      type: "success",
	      showCancelButton: true,
	      confirmButtonClass: "btn-danger",
	      confirmButtonText: "Aceptar",
	      cancelButtonText: "Cancelar",
	      closeOnConfirm: false,
	      closeOnCancel: false
	      },
	      function(isConfirm) {
	      if (isConfirm) {
	        location.reload();
	      } else {
	        location.reload();
	      }
	    });
	</script>';
}else{
   echo '<script>swal("ERROR", "Ocurrió un error inesperado, por favor intente nuevamente", "error");</script>'; 
}

