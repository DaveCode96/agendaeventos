<?php
     include '../library/configServer.php';
     include '../library/consulSQL.php';
     
   $codeProd=consultasSQL::clean_string($_POST['prod-codigo']);

    $cons=ejecutarSQL::consultar("SELECT * FROM eventos WHERE CodigoEvento='$codeProd'");
    $tmp=mysqli_fetch_array($cons, MYSQLI_ASSOC);
    
        if(consultasSQL::DeleteSQL('eventos', "CodigoEvento='".$codeProd."'")){
            $imagen=$tmp['Imagen'];
            $carpeta='../assets/img-products/';
            $directorio=$carpeta.$imagen;
            if(is_file($directorio)){
              chmod($directorio, 0777);
              unlink($directorio);
            }
            echo '<script>
                swal({
                  title: "Evento eliminado",
                  text: "El evento se eliminó con éxito del calendario",
                  type: "success",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Aceptar",
                  cancelButtonText: "Cancelar",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) {
                    location.reload();
                  } else {
                    location.reload();
                  }
                });
            </script>';
        }else{
            echo '<script>swal("ERROR", "Ocurrió un error inesperado, por favor intente nuevamente", "error");</script>'; 
        }
    
    
    mysqli_free_result($cons);