<?php
    session_start();
    include '../library/configServer.php';
    include '../library/consulSQL.php';
    include 'config.php';
    include 'funciones.php';
    $codeProd=consultasSQL::clean_string($_POST['prod-codigo']);
    $nameProd=consultasSQL::clean_string($_POST['prod-name']);
    $apeProd=consultasSQL::clean_string($_POST['prod-apellido']);
    $proceProd=consultasSQL::clean_string($_POST['prod-procedencia']);
    $phoneProd=consultasSQL::clean_string($_POST['prod-phone']);
    $emailProd=consultasSQL::clean_string($_POST['prod-email']);
    $coorProd=consultasSQL::clean_string($_POST['prod-coordinador']);
    $ubicacionProd=consultasSQL::clean_string($_POST['prod-ubicacion']);
    $edificioProd=consultasSQL::clean_string($_POST['prod-edificio']);
    $plantaProd=consultasSQL::clean_string($_POST['prod-planta']);
    
    $titleProd=consultasSQL::clean_string($_POST['title']);
    $eventProd=consultasSQL::clean_string($_POST['event']);
    $claseProd=consultasSQL::clean_string($_POST['class']);


     // Recibimos el fecha de inicio y la fecha final desde el form

    $inicio = _formatear($_POST['from']);
        // y la formateamos con la funcion _formatear

    $final  = _formatear($_POST['to']);

    $inicionormalProd=consultasSQL::clean_string($_POST['from']);
    $finalnormalProd=consultasSQL::clean_string($_POST['to']);

    







    $imgName=$_FILES['img']['name'];
    $imgType=$_FILES['img']['type'];
    $imgSize=$_FILES['img']['size'];
    $imgMaxSize=5120;

    if($codeProd!="" && $nameProd!="" && $apeProd!="" && $proceProd!="" && $phoneProd!="" && $emailProd!="" && $coorProd!="" && $ubicacionProd!="" && $edificioProd!="" && $plantaProd!="" && $titleProd!="" && $eventProd!="" && $claseProd!=""  && $inicionormalProd!="" && $finalnormalProd!=""){
        $verificar=  ejecutarSQL::consultar("SELECT * FROM eventos WHERE CodigoEvento='".$codeProd."'");//Condifcion Para el codigo unico de evento
        $verificaltotal = mysqli_num_rows($verificar);
        if($verificaltotal<=0){
            if($imgType=="image/jpeg" || $imgType=="image/png"){
                if(($imgSize/1024)<=$imgMaxSize){
                    chmod('../assets/img-products/', 0777);
                    switch ($imgType) {
                      case 'image/jpeg':
                        $imgEx=".jpg";
                      break;
                      case 'image/png':
                        $imgEx=".png";
                      break;
                    }
                    $imgFinalName=$codeProd.$imgEx;
                    if(move_uploaded_file($_FILES['img']['tmp_name'],"../assets/img-products/".$imgFinalName)){
                        if(consultasSQL::InsertSQL("eventos", "CodigoEvento, NombrePonente, ApellidoPonente, Procedencia, Telefono, Email, NIT, Ubicacion, Edificio, Planta, title, body, class, start, end, inicio_normal, final_normal, Imagen ", "'$codeProd','$nameProd','$apeProd','$proceProd','$phoneProd','$emailProd','$coorProd','$ubicacionProd','$edificioProd','$plantaProd','$titleProd','$eventProd','$claseProd','$inicio','$final','$inicionormalProd','$finalnormalProd','$imgFinalName'")){
                            echo '<script>
                                swal({
                                  title: "Evento registrado",
                                  text: "El evento se añadió al calendario con éxito",
                                  type: "success",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "Aceptar",
                                  cancelButtonText: "Cancelar",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                  },
                                  function(isConfirm) {
                                  if (isConfirm) {
                                    location.reload();
                                  } else {
                                    location.reload();
                                  }
                                });
                            </script>';
                        }else{
                            echo '<script>swal("ERROR", "Ocurrió un error inesperado, por favor intente nuevamente", "error");</script>';
                        }   
                    }else{
                        echo '<script>swal("ERROR", "Ha ocurrido un error al cargar la imagen", "error");</script>';
                    }  
                }else{
                    echo '<script>swal("ERROR", "Ha excedido el tamaño máximo de la imagen, tamaño máximo es de 5MB", "error");</script>';
                }
            }else{
                echo '<script>swal("ERROR", "El formato de la imagen del producto es invalido, solo se admiten archivos con la extensión .jpg y .png ", "error");</script>';
            }
        }else{
            echo '<script>swal("ERROR", "El Codigo de evento que acaba de ingresar ya está registrado en el sistema, por favor ingrese otro evento de calendario o recargue la pagina", "error");</script>';
        }
    }else {
        echo '<script>swal("ERROR", "Los campos no deben de estar vacíos, por favor verifique e intente nuevamente", "error");</script>';
    }


  // Obtenemos el ultimo id insetado
        $im=$conexion->query("SELECT MAX(id) AS id FROM eventos");
        $row = $im->fetch_row();  
        $id = trim($row[0]);

        // para generar el link del evento
        $link = "$base_url"."descripcion_evento.php?id=$id";

        // y actualizamos su link
        $query="UPDATE eventos SET url = '$link' WHERE id = $id";

        // Ejecutamos nuestra sentencia sql
        $conexion->query($query); 
