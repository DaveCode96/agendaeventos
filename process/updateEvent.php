<?php
session_start();
include '../library/configServer.php';
include '../library/consulSQL.php';
 include 'config.php';
    include 'funciones.php';
$codeOldProdUp=consultasSQL::clean_string($_POST['prod-codigoOld']);


    $nameProdUp=consultasSQL::clean_string($_POST['prod-name']);
    $apeProdUp=consultasSQL::clean_string($_POST['prod-apellido']);
    $proceProdUp=consultasSQL::clean_string($_POST['prod-procedencia']);
    $phoneProdUp=consultasSQL::clean_string($_POST['prod-phone']);
    $emailProdUp=consultasSQL::clean_string($_POST['prod-email']);
    $coorProdUp=consultasSQL::clean_string($_POST['prod-coordinador']);
     $ubicacionProdUp=consultasSQL::clean_string($_POST['prod-ubicacion']);
    $edificioProdUp=consultasSQL::clean_string($_POST['prod-edificio']);
    $plantaProdUp=consultasSQL::clean_string($_POST['prod-planta']);




    $titleProdUp=consultasSQL::clean_string($_POST['title']);
    $eventProdUp=consultasSQL::clean_string($_POST['event']);
    $claseProdUp=consultasSQL::clean_string($_POST['class']);


     // Recibimos el fecha de inicio y la fecha final desde el form

    $inicio = _formatear($_POST['from']);
        // y la formateamos con la funcion _formatear

    $final  = _formatear($_POST['to']);

    $inicionormalProdUp=consultasSQL::clean_string($_POST['from']);
    $finalnormalProdUp=consultasSQL::clean_string($_POST['to']);














$imgName=$_FILES['img']['name'];
$imgType=$_FILES['img']['type'];
$imgSize=$_FILES['img']['size'];
$imgMaxSize=5120;

if($imgName!=""){
  if($imgType=="image/jpeg" || $imgType=="image/png"){
    if(($imgSize/1024)<=$imgMaxSize){
        chmod('../assets/img-products/', 0777);
        switch ($imgType) {
          case 'image/jpeg':
            $imgEx=".jpg";
          break;
          case 'image/png':
            $imgEx=".png";
          break;
        }
        $imgFinalName=$codeOldProdUp.$imgEx;
        if(!move_uploaded_file($_FILES['img']['tmp_name'],"../assets/img-products/".$imgFinalName)){
            echo '<script>swal("ERROR", "Ha ocurrido un error al cargar la imagen", "error");</script>';
            exit();
        }
    }else{
        echo '<script>swal("ERROR", "Ha excedido el tamaño máximo de la imagen, tamaño máximo es de 5MB", "error");</script>';
        exit();
    }
  }else{
    echo '<script>swal("ERROR", "El formato de la imagen del producto es invalido, solo se admiten archivos con la extensión .jpg y .png ", "error");</script>';
    exit();
  }
}

if(consultasSQL::UpdateSQL("eventos", "NombrePonente='$nameProdUp', ApellidoPonente='$apeProdUp',Procedencia='$proceProdUp',Telefono='$phoneProdUp',Email='$emailProdUp',NIT='$coorProdUp',Ubicacion='$ubicacionProdUp',Edificio='$edificioProdUp',Planta='$plantaProdUp',title='$titleProdUp',body='$eventProdUp',class='$claseProdUp',start='$inicio',end='$final',inicio_normal='$inicionormalProdUp',final_normal='$finalnormalProdUp'", "CodigoEvento='$codeOldProdUp'")){
   echo '<script>
    swal({
      title: "Evento actualizado",
      text: "El Evento se actualizo con éxito",
      type: "success",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
      closeOnConfirm: false,
      closeOnCancel: false
      },
      function(isConfirm) {
      if (isConfirm) {
        location.reload();
      } else {
        location.reload();
      }
    });
  </script>';
}else{
    echo '<script>swal("ERROR", "Ocurrió un error inesperado, por favor intente nuevamente", "error");</script>';
}