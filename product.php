<?php
                  include 'library/configServer.php';
                  include 'library/consulSQL.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Productos</title>
    <?php include './inc/link.php'; ?>
</head>
<body id="container-page-product">
    <?php include './inc/navbar.php'; ?>
    <section id="store">
       <br>
        <div class="container">
            <div class="page-header">
              <h1>Eventos <small class="tittles-pages-logo">Upt</small></h1>
            </div>
            <?php
              $checkAllCat=ejecutarSQL::consultar("SELECT * FROM eventos");
              if(mysqli_num_rows($checkAllCat)>=1):
            ?>

   <div class="container">

            <a href="product.php?categ=event-info"><button class="btn btn-info btn-raised" type="submit">Academicos / Cursos</button></a>
            <a href="product.php?categ=event-success"><button class="btn btn-info btn-raised" type="submit">Culturales / Deportivos</button></a>
             <a href="product.php?categ=event-important"><button class="btn btn-info btn-raised" type="submit">Conferencias / Concursos</button></a>
              <a href="product.php?categ=event-warning"><button class="btn btn-info btn-raised" type="submit">Convenios / Graduaciones</button></a>
               <a href="product.php?categ=event-special"><button class="btn btn-info btn-raised" type="submit"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspOtros&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button></a>


  </div>

                  <div class="col-xs-12 col-md-4 col-md-offset-4">
                    <form action="./search.php" method="GET">
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
                          <input type="text" id="addon1" class="form-control" name="term" required="" title="Escriba class o evento o codigo del producto">
                          <span class="input-group-btn">
                              <button class="btn btn-info btn-raised" type="submit">Buscar</button>
                          </span>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

            <?php
                $eventos=consultasSQL::clean_string($_GET['categ']);
                if(isset($eventos) && $eventos!=""){
            ?>
              <div class="row">
                <?php
                  $mysqli = mysqli_connect(SERVER, USER, PASS, BD);
                  mysqli_set_charset($mysqli, "utf8");

                  $pagina = isset($_GET['pag']) ? (int)$_GET['pag'] : 1;
                  $regpagina = 20;
                  $inicio = ($pagina > 1) ? (($pagina * $regpagina) - $regpagina) : 0;

                  $consultar_productos=mysqli_query($mysqli,"SELECT SQL_CALC_FOUND_ROWS * FROM eventos WHERE class='$eventos'  LIMIT $inicio, $regpagina");

                  $selCat=ejecutarSQL::consultar("SELECT * FROM eventos WHERE class='$eventos'");
                  $datCat=mysqli_fetch_array($selCat, MYSQLI_ASSOC);

                  $totalregistros = mysqli_query($mysqli,"SELECT FOUND_ROWS()");
                  $totalregistros = mysqli_fetch_array($totalregistros, MYSQLI_ASSOC);
        
                  $numeropaginas = ceil($totalregistros["FOUND_ROWS()"]/$regpagina);

                  if(mysqli_num_rows($consultar_productos)>=1){
                    echo '<h3 class="text-center">Se muestran los eventos de la categoría <strong>"selecionada"</strong></h3><br>';
                    while($prod=mysqli_fetch_array($consultar_productos, MYSQLI_ASSOC)){
                ?>
                <div class="container">
                     <div class="col-xs-12 col-sm-6 col-md-4">
                     <div >
                      <img  width="350" height="200" src="assets/img-products/<?php if($prod['Imagen']!="" && is_file("./assets/img-products/".$prod['Imagen'])){ echo $prod['Imagen']; }else{ echo "default.png"; } ?>">
                       <div class="caption">
                            <h4><?php echo $prod['title']; ?></h4>
                            <h5>Ponente:<?php echo $prod['NombrePonente']. " ".$prod['ApellidoPonente']; ?></h5>
                            <p>Ubicacion:<?php echo $prod['Ubicacion']." ".$prod['Edificio']." ".$prod['Planta']; ?></p>
                            
                             </p>
                           

                        <p class="text-center">
                            <a href="infoProd.php?CodigoEvento=<?php echo $prod['CodigoEvento']; ?>" class="btn btn-primary btn-sm btn-raised btn-block"><i class="fa fa-plus"></i>&nbsp; Detalles</a>
                        </p>
                       </div>
                     </div>
                </div>   

                <?php    
                  }
                  if($numeropaginas>0):
                ?>
                <div class="clearfix"></div>
                <div class="text-center">
                  <ul class="pagination">
                    <?php if($pagina == 1): ?>
                        <li class="disabled">
                            <a>
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="product.php?categ=<?php echo $eventos; ?>&pag=<?php echo $pagina-1; ?>">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                    <?php endif; ?>


                    <?php
                        for($i=1; $i <= $numeropaginas; $i++ ){
                            if($pagina == $i){
                                echo '<li class="active"><a href="product.php?categ='.$eventos.'&pag='.$i.'">'.$i.'</a></li>';
                            }else{
                                echo '<li><a href="product.php?categ='.$eventos.'&pag='.$i.'">'.$i.'</a></li>';
                            }
                        }
                    ?>
                    

                    <?php if($pagina == $numeropaginas): ?>
                        <li class="disabled">
                            <a>
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="product.php?categ=<?php echo $eventos; ?>&pag=<?php echo $pagina+1; ?>">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    <?php endif; ?>
                  </ul>
                </div>
                <?php
                  endif;
                  }else{
                    echo '<h2 class="text-center">Lo sentimos, no hay eventos registrados en la categoría <strong>"'.$datCat['class'].'"</strong></h2>';
                  }
                ?>
              </div>
            <?php
                }else{
                  echo '<h2 class="text-center">Por favor seleccione una categoría para empezar</h2>';
                }
              else:
                echo '<h2 class="text-center">Lo sentimos, no hay eventos ni categorías registradas en el sistema</h2>';
              endif;
            ?>

        </div>




    </section>
    <?php include './inc/footer.php'; ?>
</body>
</html>